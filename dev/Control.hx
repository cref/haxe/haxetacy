package dev;

/**
 * ...
 * @author Cref
 */

//@:autoBuild(nl.haaglanden.MyMacro.build()) 
class Control < TElm:HTMLElement > {
	
	public function new(controller:Control<HTMLElement>) {
		if (element==null) element = controller.build(getElementStructure());
	}
	function getElementStructure():Dynamic return { span:[] }
	public var element(default,null):TElm;
}