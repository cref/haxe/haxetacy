package dev;

/**
 * ...
 * @author Cref
 */

class FunctionOperators {

	public static function and(fn1:Void->Bool, fn2:Void->Bool):Void->Bool return function() return fn1()&&fn2()
	public static function or(fn1:Void->Bool, fn2:Void->Bool):Void->Bool return function() return fn1()||fn2()
	public static function not(fn1:Void->Bool):Void->Bool return function() return !fn1()
	//xor?
}