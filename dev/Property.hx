package dev;

/**
 * a class that enables you to bind functions to property changes:
	 * myEventScope.before(myProp.change,myFunction)
	 * myEventScope.after(myProp.change,myFunction)
	 * myEventScope.updateUI([myProp1.change,myProp2.change],myFunction)
 * @author Cref
 */

class Property<T> {
	var v:T;
	var n:T;
	public function new(?value:T) v=value
	public function set(value:T):T {
		if (value != v) {
			n = value;
			change();
			n = null;
		}
		return v;
	}
	public var value(get, set):T;
	public inline function get():T return v
	public function change():Void v = n
	function toString() return ''+v
	function valueOf() return v
}