package dev;

/**
 * ...
 * @author Cref
 */
using hxtc.use.ForString;

class Utils {

	/*
	returns the value for a given alias, example:
	matchAlias(
		'no alias1 alias2 alias3\n'+
		'yes alias4 alias5 alias 6\n'+
		'nope alias7 alias8 alias9\n',
		'alias5'
	);//returns 'yes'
	*/
	public static function matchAlias(str:String,alias:String):String {
		var re = new RegExp('\\n(\\S*)[^\\n]*? '+alias.escapeERegChars()+'\\s','i');
		var match = re.exec('\n'+str+'\n');
		return match==null?null:match[1];
	}
}