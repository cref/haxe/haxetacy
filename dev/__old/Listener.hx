package hxtc.events;
import hxtc.Tools;
import jstm.Host;

using ES5;

/**
 * Example usage:
 * fn=function() untyped alert(myObj.title+': '+myObj.color);
 * new Listener(fn,1000).listen(myObj.setColor).listen(myObj.setTitle).disable();
 * @author Cref
 */


/*
class OLDListener {
	
	public static var defaultDelay(default, setDefaultDelay):Int;
	static function setDefaultDelay(n:Int):Int {
		//return Listener.getPrototype().delay = n;
	}
	static function __init__() {
		defaultDelay = 100;
	}
	
	var delay:Int;
	var triggers:ObjectHash<Dynamic>;

	public function new(fn:Void->Void,?delay:Int):Void {
		/*
		Tools.patch(fn,function(fn) {
			return function() {
				untyped alert(2);
				fn();
			};
		});
		* /
		this.delay = delay == null?defaultDelay:delay;
		triggers = new ObjectHash();
	}
	
	inline function getClosure<T>(fn:T):Closure<T> return cast fn
	
	public function add(closure:Dynamic):Listener {
		if (!triggers.exists(closure)) {
			var c:Closure < Void->Void >= cast closure;
			//if (!c.method.h)
			Tools.patch(closure, function() {
				
			});
			triggers.add(closure);
		}
		return this;
	}
	
	public function enable():Listener {
		return this;
	}
	
	public function disable():Listener {
		return this;
	}
	
	public var isActive(default, setIsActive):Bool;
	function setIsActive(b:Bool):Bool {
		b?enable():disable();
		return b;
	}
	
}
*/