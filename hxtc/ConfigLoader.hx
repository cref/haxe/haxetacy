package hxtc;

using ES5;
/**
* ...
* @author Cref
*/

class ConfigLoader implements Dynamic<String> {

	public function new(config:String,?preset:Dynamic<String>):Void {
		#if debug
		//loading debug settings made easy
		set("debug", "-debug");
		#end
		if (preset != null) Tools.ext(this, preset);
		//if (preset != null) for (key in Reflect.fields(preset)) set(key, Reflect.field(preset, key));
		load(config);
	}
	
	public function load(config:String):Void {
		for (pair in config.split("\n")) {
			pair = resolveRefs(pair);
			var sep = pair.indexOf("=");
			if (sep > 0) {
				var key = pair.substr(0, sep), value = pair.substr(sep + 1);
				sep = key.indexOf('?');
				if (sep > -1) {
					if (get(key.substr(0, sep))==null) continue;
					key = key.substr(sep + 1);
				}
				value==''?this.get(key).delete():set(key,value);
			}
		}
	}
	
	function resolveRefs(v:String):String {
		while(valueRef.match(v)) v = valueRef.customReplace(v,replaceRef);
		return v;
	}
	
	function replaceRef(er:EReg):String return get(er.matched(1))
	
	static var valueRef = ~/{=([^{]*?)}/;
}


class HierarchicalConfigLoader extends ConfigLoader {
	
	public function new(startAt:String,path:Array<String>,name:String,preset:Dynamic<String>):Void {
		if(startAt == null) startAt = haxe.Sys.getCwd();
		super(getPathCfg([startAt].concat(path),name),preset);
	}
	
	function getPathCfg(path:Array<String>,name:String):String {
		var cfg = [], p = "";
		for (part in path) {
			p += part + "/";
			cfg.push("currentFolder=" + p);
			var f = p + "bin/" + name + ".cfg";
			if (haxe.FileSystem.exists(f)) cfg.push(StringTools.replace(haxe.io.File.getContent(f), "\r", ""));
		}
		return cfg.join("\n");
	}
	
}