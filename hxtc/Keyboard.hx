/**
 * ...
 * @author Cref
 */

package hxtc;

extern class Keyboard {
	
	public static inline var backspace = 8;
	public static inline var tab = 9;
	public static inline var enter = 13;
	public static inline var shift = 16;
	public static inline var ctrl = 17;
	public static inline var alt = 18;
	public static inline var pause = 19;
	public static inline var capslock = 20;
	public static inline var escape = 27;
	public static inline var space = 32;
	public static inline var pageUp = 33;
	public static inline var pageDown = 34;
	public static inline var end = 35;
	public static inline var home = 36;
	public static inline var left = 37;
	public static inline var up = 38;
	public static inline var right = 39;
	public static inline var down = 40;
	public static inline var printScreen = 44;
	public static inline var insert = 45;
	public static inline var delete = 46;
	
	//numbers
	public static inline var zero = 48;
	public static inline var one = 49;
	public static inline var two = 50;
	public static inline var three = 51;
	public static inline var four = 52;
	public static inline var five = 53;
	public static inline var six = 54;
	public static inline var seven = 55;
	public static inline var eight = 56;
	public static inline var nine = 57;
	
	//letters
	public static inline var a = 65;
	public static inline var b = 66;
	public static inline var c = 67;
	public static inline var d = 68;
	public static inline var e = 69;
	public static inline var f = 70;
	public static inline var g = 71;
	public static inline var h = 72;
	public static inline var i = 73;
	public static inline var j = 74;
	public static inline var k = 75;
	public static inline var l = 76;
	public static inline var m = 77;
	public static inline var n = 78;
	public static inline var o = 79;
	public static inline var p = 80;
	public static inline var q = 81;
	public static inline var r = 82;
	public static inline var s = 83;
	public static inline var t = 84;
	public static inline var u = 85;
	public static inline var v = 86;
	public static inline var w = 87;
	public static inline var x = 88;
	public static inline var y = 89;
	public static inline var z = 90;
	
	//special keys
	public static inline var commandLeft = 91;
	public static inline var commandRight = 92;
	public static inline var application = 93;
	
	//numpad
	public static inline var num0 = 96;
	public static inline var num1 = 97;
	public static inline var num2 = 98;
	public static inline var num3 = 99;
	public static inline var num4 = 100;
	public static inline var num5 = 101;
	public static inline var num6 = 102;
	public static inline var num7 = 103;
	public static inline var num8 = 104;
	public static inline var num9 = 105;
	public static inline var times = 106;
	public static inline var plus = 107;
	public static inline var minus = 109;
	public static inline var point = 110;
	public static inline var divide = 111;
	
	//function keys
	public static inline var f1 = 112;
	public static inline var f2 = 113;
	public static inline var f3 = 114;
	public static inline var f4 = 115;
	public static inline var f5 = 116;
	public static inline var f6 = 117;
	public static inline var f7 = 118;
	public static inline var f8 = 119;
	public static inline var f9 = 120;
	public static inline var f10 = 121;
	public static inline var f11 = 122;
	public static inline var f12 = 123;
	
	public static inline var numlock = 144;
	public static inline var scrolllock = 145;
	
	public static inline var semicolon = 186;
	public static inline var equal = 187;
	public static inline var comma = 188;
	public static inline var dash = 189;
	public static inline var period = 190;
	public static inline var forwardSlash = 191;
	public static inline var accentGrave = 192;
	public static inline var openBracket = 219;
	public static inline var backSlash = 220;
	public static inline var closeBracket = 221;
	public static inline var quote = 222;
	
}