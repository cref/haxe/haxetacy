package hxtc;

using Reflect;
/**
 * ...
 * @author Cref
 */
typedef PathNodeHandler = Array<String>->Dynamic<PathNodeHandler>;

class PathHandler<T> {

	public static function exec(path:Array<String>, handlers:Dynamic<PathNodeHandler>, index:Int = 0) {
		var fn = handlers.field(path[index]);
		if (fn == null) fn = handlers.field('_');
		if (!Reflect.isFunction(fn)) return;
		//make sure the function gets called on the right context
		var r=Reflect.callMethod(handlers,fn,[path.slice(0,index+=1)]);
		if (r != null) exec(path,r,index);
	}
	
}