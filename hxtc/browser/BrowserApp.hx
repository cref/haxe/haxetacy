package hxtc.browser;
#if browser
import jstm.Host;

/**
 * ...
 * @author Cref
 */

class BrowserApp {

	static var ready:Bool;
	static var fnArr:Array<Void->Void>;
	static dynamic function onReady() {
		onReady = function() { };
		for (fn in fnArr) fn();
		ready = true;
	}
	static function __init__() {
		var w = Host.window,d=w.document;
		ready = false;
		fnArr = [];
		//if (untyped __js__("!('readyState' in document)")) 
		d.addEventListener("DOMContentLoaded", function(e)onReady(), false);
		//else {
			var dE = d.documentElement;
			var test = untyped dE.doScroll
				?function() try return !dE.doScroll('up') catch (e:Dynamic) return false
				:untyped __js__("'readyState' in document")
					?function() return d.readyState == 'complete'
					:function() return d.body && d.body.style.display!=null
			;
			(function() test()?onReady():w.setTimeout(cast ES5.arguments.callee,10))();
		//}
	}
	
	public static function whenDOMReady(fn:Void->Void):Void ready?fn():fnArr.push(fn)
	
}
#end