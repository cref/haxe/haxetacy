/**
 * ...
 * @author Cref
 */

package hxtc.db;

typedef ConnectionParameters = {
	host : String,
	port : Int,
	user : String,
	pass : String,
	socket : String,
	database : String
}