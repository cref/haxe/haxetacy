package hxtc.dom.controls;
import jstm.Host;
import hxtc.dom.style.Selector;


using hxtc.use.ForString;
using hxtc.dom.style.Selector;
using hxtc.dom.DOMTools;
/**
 * ...
 * @author Cref
 */

class Application extends ContentBox {
	
	static var embedSelector = Selector.getControlSelector().descendants(new Selector('embed'));// .writeStyle('backgroundColor', 'green');// .writeStyle('display', 'none');// ;
	
	public function new(w:Window) {
		super(w.document);
		window = w;
		var t = this;
		//who needs DOMContentLoaded?
		(function() w.document.body!=null?t.start():w.setTimeout(cast ES5.arguments.callee,0))();
		//window.document.body == null?window.document.addEventListener('load', onload, false):onload();
		//TODO: use DOMContentLoaded with fallback instead of load
		//window.document.addEventListener('DOMContentLoaded', onload, false);
		//window.document.addEventListener('DOMNodeInserted', function(e) untyped console.log(5), false);
		embeds = new Hash();
		embedClasses = new Hash();
	}
	
	
	
	public var embedClasses:Hash<String>;
	var embeds:Hash<HTMLElement>;
	
	//checks for any embedded controls that need redrawing
	//TODO: make recursive so embeds can also contain embeds
	function redraw() {
		var placeholders = element.querySelectorAll('embed'),t=this;
		for (e in placeholders) {
			if (!embedClasses.exists(e.id)) continue;
			if (embeds.exists(e.id)) embeds.get(e.id).swap(e);
			else {
				e.style.display = 'none';
				jstm.Runtime.useClass(embedClasses.get(e.id), function(C:Class<hxtc.dom.UIControl<Dynamic>>) {
					var ctrl:hxtc.dom.UIControl<Dynamic> = untyped __new__(C, t);
					var ce:HTMLElement = t.doc.createElement(ctrl.tagName);
					ce.id = e.id;
					t.embeds.set(ce.id,ce);
					ctrl.bind(cast ce.swap(e));
				});
			}
			//var c = doc.buildElement({div:['zoekie!'],style:'border:2px solid green'}).swap(e);
			//if (ctrl.id != '') lookupIdInConfig();
			//var cn = ctrl.className.split(' ')[1];
			//ctrl.innerText = ctrl.id == ''?cn:ctrl.id;//Reflect.field(getConfig(), ctrl.id)._;
			//ctrl.className = cn;
		}
	}
	
	function getConfig():Dynamic{}
	
	function start():Void {}
	
	public var window(default, null):Window;
}





/* http://www.kryogenix.org/days/2007/09/26/shortloaded

function(i) {
	var u = navigator.userAgent;
	var e =/*@cc_on!@* /false; 
	var st = setTimeout;
	if (/webkit/i.test(u)) st(function() {
		var dr=document.readyState;
		dr == "loaded" || dr == "complete"?i():st(arguments.callee, 10);
	},10);
	else if((/mozilla/i.test(u)&&!/(compati)/.test(u)) || (/opera/i.test(u))) document.addEventListener("DOMContentLoaded", i, false);
	else if(e) (function() {
		try {
			i(document.documentElement.doScroll('up'););
		} catch (e) {
			st(arguments.callee, 10);
		}
	})();
	else window.onload = i;
}
*/

/* http://javascript.nwbox.com/IEContentLoaded/
 
// @w	window reference
// @fn	function reference
function IEContentLoaded (w, fn) {
	var d = w.document, done = false,
	// only fire once
	init = function () {
		if (!done) {
			done = true;
			fn();
		}
	};
	// polling for no errors
	(function () {
		try {
			// throws errors until after ondocumentready
			d.documentElement.doScroll('up');
		} catch (e) {
			setTimeout(arguments.callee, 50);
			return;
		}
		// no errors, fire
		init();
	})();
	// trying to always fire before onload
	d.onreadystatechange = function() {
		if (d.readyState == 'complete') {
			d.onreadystatechange = null;
			init();
		}
	};
}
*/