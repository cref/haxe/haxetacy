/**
 * ...
 * @author Cref
 */

package hxtc.orm;

typedef Order = {
	field:Selectable<Dynamic>,
	desc:Bool
}